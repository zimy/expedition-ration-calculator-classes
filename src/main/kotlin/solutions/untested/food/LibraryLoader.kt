package solutions.untested.food

import com.fasterxml.jackson.databind.ObjectMapper

import com.fasterxml.jackson.databind.type.MapType
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.registerKotlinModule


val mapper = ObjectMapper(YAMLFactory()).registerKotlinModule()
val hashMapStringToInt: MapType = mapper.typeFactory.constructMapType(Map::class.java, String::class.java, Int::class.java)

object LibraryLoader {
    fun prettyPrint(): String = Products.values()
        .groupingBy { it.group }
        .fold("", operation = { accumulator: String, element: Products ->
            accumulator + (if (accumulator.isNotBlank()) "; " else "") + element.russianName
        }).prettyPrint() + "\nБлюда: " + items.values.sortedBy { it.name }.map { it.name }.joinToString("; \n")

    val items: Map<String, CousinMenuItem> = Meal.values().map { Pair(it.russianName, it.toCousinMenuItem()) }
        .plus(JSONMeals.values().map { it.russianName }.map {
            Pair(
                it,
                CousinMenuItem(
                    it,
                    readMapFromFile(it)
                )
            )
        }).toMap()

    private fun readMapFromFile(russianName: String): Map<Products, Int> {
        val resource = javaClass.getResource("/$russianName.yml")
        if (resource==null) {
            System.err.println("Resource $russianName.yml not found")
        }
        return mapper.readValue<Map<String, Int>>(resource, hashMapStringToInt).map {
            val product = Products.valueOf(it.key)
            if (product == null) {
                System.err.println("Product ${it.key} not found")
            }
            Pair(product, it.value)
        }.toMap()
    }
}