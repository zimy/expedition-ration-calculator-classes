package solutions.untested.food

data class CousinMenu(val name: String, val days: List<CousinMenuDay>, val everydayBonus: CousinMenuItem) {
    fun BOM(persons: Int): List<BOMItem> {
        return days
            .flatMap { listOf(it.first, it.second, it.third, it.pocket, everydayBonus) }
            .flatMap { it.components.entries }
            .groupingBy { it.key.name }
            .fold(0, operation = { accumulator: Int, element: Map.Entry<Products, Int> ->
                accumulator + element.value * persons
            })
            .map { entry -> BOMItem(entry.key, entry.value) }
            .sortedBy { it.name }
    }

    fun weight(persons: Int) = (days.map { it.weight() }.sum() + everydayBonus.weight() * days.size) * persons
    fun weights() = days.map { it.weight() + everydayBonus.weight() }.toList()
    fun wrapPair(persons: Int) = Pair(this, persons)

    fun prettyPrint(size: Int) =
        days.mapIndexed { index, cousinMenuDay ->
            """Меню на ночёвку ${index+1}:
${cousinMenuDay.prettyPrint(size)}
        """.trimIndent()
        }.joinToString(separator = "\n")

}

data class CousinMenuDay(
    val first: CousinMenuItem,
    val second: CousinMenuItem,
    val third: CousinMenuItem,
    val pocket: CousinMenuItem,
    val name: String = ""
) {
    fun toPFCC() = first.toPFCC() + second.toPFCC() + third.toPFCC() + pocket.toPFCC()
    fun weight() = first.weight() + second.weight() + third.weight() + pocket.weight()
    fun oneDayMenu(everydayBonus: CousinMenuItem) = CousinMenu(name, listOf(this), everydayBonus)

    fun prettyPrint(size: Int): String {


        val prettyPrint1 = first.prettyPrint(size)
        val prettyPrint2 = second.prettyPrint(size)
        val prettyPrint3 = third.prettyPrint(size)
        val prettyPrint4 = pocket.prettyPrint(size)

        return (if (prettyPrint1.isNotEmpty())  "    ужин:    $prettyPrint1\n" else "") +
                (if (prettyPrint2.isNotEmpty()) "    завтрак: $prettyPrint2\n" else "") +
                (if (prettyPrint3.isNotEmpty()) "    обед:    $prettyPrint3\n" else "") +
                (if (prettyPrint4.isNotEmpty()) "    перекус: $prettyPrint4\n" else "")
    }

}

fun CousinMenuDay.oneDayMenu(everydayBonus: String) = this.oneDayMenu(item(everydayBonus))
fun CousinMenuDay.oneDayMenu() = this.oneDayMenu(Meal.заглушка.toCousinMenuItem())

data class CousinMenuItem(val name: String, val components: Map<Products, Int>) {
    fun toPFCC() = PFCC(
        components.entries.map { it.key.proteins * it.value / 100 }.sum(),
        components.entries.map { it.key.fats * it.value / 100 }.sum(),
        components.entries.map { it.key.carbohydrates * it.value / 100 }.sum(),
        components.entries.map { it.key.calories * it.value / 100 }.sum()
    )

    fun weight() = components.values.sum()

    fun prettyPrint(size: Int) =
        name + if (components.entries.isNotEmpty()) {
            " (" + (components.entries.sortedBy { it.key.group }
                .joinToString(", ") { it.key.name + " " + (it.value * size) }) + ")"
        } else ""
}

data class PFCC(val proteins: Float, val fats: Float, val carbohydrates: Float, val calories: Float) {
    operator fun plus(increment: PFCC): PFCC {
        return PFCC(
            proteins + increment.proteins,
            fats + increment.fats,
            carbohydrates + increment.carbohydrates,
            calories + increment.calories
        )
    }
}

data class BOMItem(val name: String, val weight: Int) {
    fun prettyPrint() = "$name: $weight"
}


fun day(name: String, first: String, second: String, third: String, pocket: String): CousinMenuDay {
    val firstItem = LibraryLoader.items[first]!!
    val secondItem = LibraryLoader.items[second]!!
    val thirdItem = LibraryLoader.items[third]!!
    val pocketItem = LibraryLoader.items[pocket]!!
    return CousinMenuDay(firstItem, secondItem, thirdItem, pocketItem, name)
}

fun List<BOMItem>.merge() = this.groupingBy { it.name }
    .fold(0, operation = { accumulator: Int, element: BOMItem ->
        accumulator + element.weight
    })
    .map { entry -> BOMItem(entry.key, entry.value) }
    .sortedBy { it.name }

fun List<BOMItem>.sortByProductOrder() = this.sortedBy { Products.valueOf(it.name).group.order }
fun List<BOMItem>.mapToPrettyPrint() = this.map { it.prettyPrint() }
fun List<BOMItem>.prettyPrintWithSeparator(separator: CharSequence) = this.mapToPrettyPrint().joinToString(separator)
fun List<BOMItem>.prettyPrintln() = this.prettyPrintWithSeparator("\n")

enum class ProductCategory(val order: Int, val russian: String) {
    Meat(0, "Мясо"), Milk(1, "Сыры и молочное"), Grains(
        2,
        "Крупы"
    ),
    Vegetables(3, "Овощи и фрукты"), Nuts(4, "Орехи и сухофрукты"), Sweets(5, "Сладости"), Drinkable(
        6,
        "Чай и иные напитки"
    ),
    Dispersed(7, "Приправы, специи, соусы"),
    TouristShop(8, "Туристический отдел"),
    SportShop(8, "Спортивный магазин"),
    Whatever(99, "ХЗ")
}

fun Map<ProductCategory, String>.prettyPrint() =
    this.entries.sortedBy { it.key.order }.map { it.key.russian + ": " + it.value }.joinToString("\n")

fun item(name: String) = LibraryLoader.items[name]!!

fun menu(name: String, everydayBonus: String, vararg days: CousinMenuDay): CousinMenu {
    val item = item(everydayBonus)
    return CousinMenu(name, days.asList(), item)
}

fun Meal.toCousinMenuItem() = CousinMenuItem(this.russianName, this.components)

fun printMenuBOMoneLine(menuAndPeopleCount: Pair<CousinMenu, Int>) {
    println(
        menuAndPeopleCount.first.name + " " + menuAndPeopleCount.first.BOM(menuAndPeopleCount.second).mapToPrettyPrint()
    )
}

fun printAndTestOneMenu(pairOfMenuAndPeopleCount: Pair<CousinMenu, Int>): Int {
    val menuItem = pairOfMenuAndPeopleCount.first
    println(
        "Секция меню \"${menuItem.name}\":\n" +
                "веса дней (${menuItem.weights()}), общий вес ${
            menuItem.weight(
                pairOfMenuAndPeopleCount.second
            )
        }"
    )
    val testMenuStayingHome = CousinRuleEngine().testMenuStayingHome(menuItem)
    if (testMenuStayingHome != 0) {
        println("⚠\uFE0F $testMenuStayingHome")
    }
    println(menuItem.prettyPrint(pairOfMenuAndPeopleCount.second))
    return testMenuStayingHome
}