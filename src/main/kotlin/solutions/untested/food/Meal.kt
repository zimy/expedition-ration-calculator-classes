package solutions.untested.food

enum class Meal(val russianName: String, val components: Map<Products, Int>) {
    заглушка("", mapOf()),
    уха(
        "финская уха Лохикейто",
        mapOf(
            Products.Лосось to 60,
            Products.`Картофельное пюре` to 60,
            Products.Лук to 10,
            Products.Морковь to 10,
            Products.Сливки to 4,
            Products.`Масло топленое` to 5,
            Products.Сухари to 15,
            Products.Печенье to 35
        )
    ),
    пшёнкатушоночная(
        "пшёнка с тушёнкой",
        mapOf(
            Products.Пшено to 80,
            Products.`Тушенка курятина Кронидов` to 80,
            Products.Пряники to 50,
            Products.`Сыр` to 50
        )
    ),
    собасговядинойкронидов(
        "соба с говядиной кронидов",
        mapOf(
            Products.`Лапша Соба Гречневая` to 75,
            Products.`Тушенка говядина Кронидов` to 80,
            Products.Пряники to 60,
            Products.Козинак to 60
        )
    ),
    собасгкурятинойкронидов(
        "соба с курятиной кронидов",
        mapOf(
            Products.`Лапша Соба Гречневая` to 100,
            Products.`Тушенка курятина Кронидов` to 80,
            Products.Пряники to 60,
            Products.Майонез to 30,
            Products.Козинак to 60
        )
    ),
    чечевиЦакуриЦа(
        "чечевица с курятиной",
        mapOf(
            Products.Чечевица to 100,
            Products.`Тушенка курятина Кронидов` to 80,
            Products.`Конфеты батончики ротФронт` to 50,
            Products.`Козинак` to 50
        )
    ),
    кускусССыром(
        "кускус с сыром",
        mapOf(
            Products.Кускус to 80,
            Products.Сыр to 50,
            Products.Колбаса to 50,
            Products.Кисель to 15,
            Products.`Конфеты батончики ротФронт` to 30,
            Products.Мяссури to 30,
        )
    ),
    перекусДзоу2023
        (
        "перекус сыркурага",
        mapOf(
            Products.Сухари to 100,
            Products.Сыр to 50,
            Products.Колбаса to 50,
            Products.Курага to 30,
        )
    ),
    макароныСговядинойКронидов(
        "макароны с говядиной Кронидов",
        mapOf(
            Products.Макароны to 100,
            Products.`Тушенка говядина Кронидов` to 80,
            Products.`Конфеты батончики ротФронт` to 50,
            Products.Сыр to 50,
            Products.`Козинак` to 50
        )
    ),
}
