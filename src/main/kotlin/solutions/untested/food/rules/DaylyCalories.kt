package solutions.untested.food.rules

import solutions.untested.food.CousinMenu

class DaylyCalories(val caloriesLow: Int, val caloriesHigh: Int) : CousinMenuRule {
    override fun test(menu: CousinMenu): Int {
        val bonus = menu.everydayBonus.toPFCC()
        val count = menu.days.toSet().map {
            val pfcc = it.toPFCC() + bonus
            val b1 = pfcc.calories > caloriesLow
            val b2 = pfcc.calories < caloriesHigh
            var problems = 0
            if (!b1) {
                println("\"${it.name}\": недостаточно калорий (${pfcc.calories}), $caloriesLow минимум")
                problems++
            }
            if (!b2) {
                println("\"${it.name}\": много калорий (${pfcc.calories}), $caloriesHigh максимум")
                problems++
            }
            problems
        }.sum()
        return count
    }
}