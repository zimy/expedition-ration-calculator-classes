package solutions.untested.food.rules

import solutions.untested.food.CousinMenu

interface CousinMenuRule {
    fun test(menu: CousinMenu): Int
}