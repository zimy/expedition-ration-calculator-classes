package solutions.untested.food.rules

import solutions.untested.food.CousinMenu

class DaylyPFCnorms(
    private val proteinBase: Int,
    private val fatBase: Int,
    private val chBase: Int
) : CousinMenuRule {
    override fun test(menu: CousinMenu): Int {
        val bonus = menu.everydayBonus.toPFCC()
        val count = menu.days.toSet().map {
            val pfcc = it.toPFCC() + bonus
            val proteins = pfcc.proteins > proteinBase
            val fats = pfcc.fats > fatBase
            val carbohydrates = pfcc.carbohydrates > chBase
            var problems = 0
            if (!proteins) {
                println("\"${it.name}\": Недостаточно белков (${pfcc.proteins}), нужно $proteinBase ")
                problems++
            }
            if (!fats) {
                println("\"${it.name}\": Недостаточно жиров (${pfcc.fats}), нужно $fatBase")
                problems++
            }
            if (!carbohydrates) {
                println("\"${it.name}\": Недостаточно углеводов (${pfcc.carbohydrates}), нужно $chBase")
                problems++
            }
            problems
        }.sum()
        return count
    }
}